﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPractico_Modulo3
{
    class Alumno
    {
        private string nombre;
        private int edad, matricula;

        public Alumno()
        {
            this.matricula = 4596;
        }

        private void RegistroAlumno()
        {

            Console.Write("Ingrese su nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su edad: ");
            edad = int.Parse(Console.ReadLine());
        }

        protected void Datos()
        {
            Console.WriteLine("El alumno " + nombre + " de " + edad + " de edad " + " con matricula " + matricula);
        }

        public void AccesoRegistro()
        {
            RegistroAlumno();
        }

        ~Alumno()
        {
            matricula = 0;
        }
    }

    class Datos : Alumno
    {
        private string materia, grupo;

        public Datos(string g)
        {
            this.grupo = g;
        }
        
        public void Materias()
        {
            if (grupo == "A")
            {
                materia = "Calculo Integral y Programación";
            }

            if (grupo == "B")
            {
                materia = "Quimica, Programación y Fisica";
            }

            if (grupo == "C")
            {
                materia = "Algebra y Calculo Vectorial";
            }

            Console.WriteLine("Grupo " + grupo + " con las materias: " + materia);
        }

        public void AccesoHerencia()
        {
            Datos();
        }

        ~Datos()
        {
            grupo = "";
        }
    }
}
