﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPractico_Modulo3
{
    class Program
    {
        static void Main(string[] args)
        {
            string g;
            Alumno objA = new Alumno();
            objA.AccesoRegistro();

            Console.Write("Ingrese su grupo: ");
            g = Console.ReadLine();
            Datos objD = new Datos(g);
            objD.AccesoHerencia();
            objD.Materias();

            Console.ReadKey();
        }
    }
}
