﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructores
{
    class Operaciones
    {
        private int multa;

        public Operaciones(int multa)
        {
            this.multa = multa;
        }

        public int CalcularMulta()
        {
            int resultado = 0;

            switch (this.multa)
            {
                case 0:
                    resultado = 180;
                    break;

                case 1:
                    resultado = 1500;
                    break;

                case 2:
                    resultado = 1000;
                    break;

                case 3:
                    resultado = 2500;
                    break;

                default:
                    resultado = 0;
                    break;
            }

            return resultado;
        }

        ~Operaciones()
        {
            this.multa = 0;
            Console.WriteLine("Este es el destructor");
        }
    }
}
