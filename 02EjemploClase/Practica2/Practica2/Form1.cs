﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();
            TV1.setTamanio(30);
            int tamanioTV1 = TV1.getTamanio();

            TV1.setVolumen(10);
            int volumenTV1 = TV1.getVolumen();
            MessageBox.Show("El tamaño de la TV1 es: " + tamanioTV1.ToString());

            TV1.setColor("negro");
            string colorTV1 = TV1.getColor();
            MessageBox.Show("El color de la TV1 es: " + colorTV1);

            TV1.setBrillo(15);
            int brilloTV1 = TV1.getBrillo();
            MessageBox.Show("El brillo de la TV1 es: " + brilloTV1.ToString());

            TV1.setContraste(20);
            int contrasteTV1 = TV1.getContraste();
            MessageBox.Show("El contraste de la TV1 es: " + contrasteTV1.ToString());

            TV1.setMarca("LG");
            string marcaTV1 = TV1.getMarca();
            MessageBox.Show("La marca de la TV1 es: " + marcaTV1);
        }
    }
}
