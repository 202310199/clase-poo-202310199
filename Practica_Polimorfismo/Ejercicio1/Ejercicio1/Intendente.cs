﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Intendente : Empleado
    {
        public override void CalcularSalario(int h)
        {
            salario = h * 20;
        }

        public override int Salario()
        {
            return salario;
        }
    }
}
