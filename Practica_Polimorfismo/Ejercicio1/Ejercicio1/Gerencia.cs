﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Gerencia : Empleado
    {
        public override void CalcularSalario(int h)
        {
            salario = h * 120;
        }

        public override int Salario()
        {
            return salario;
        }
    }
}
