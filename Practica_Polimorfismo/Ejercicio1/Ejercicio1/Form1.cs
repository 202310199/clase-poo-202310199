﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Intendente objI = new Intendente();
        Produccion objP = new Produccion();
        Administrativo objA = new Administrativo();
        Gerencia objG = new Gerencia();
        Dueño objD = new Dueño();

        private void btnCalculaSalario_Click(object sender, EventArgs e)
        { 
            int menu = cbxEmpleado.SelectedIndex;
            switch (menu)
            {
                case 0:
                    objI.CalcularSalario(int.Parse(txtHoras.Text));
                    lblRes.Text = Convert.ToString(objI.Salario());
                    break;
                case 1:
                    objP.CalcularSalario(int.Parse(txtHoras.Text));
                    lblRes.Text = Convert.ToString(objP.Salario());
                    break;
                case 2:
                    objA.CalcularSalario(int.Parse(txtHoras.Text));
                    lblRes.Text = Convert.ToString(objA.Salario());
                    break;
                case 3:
                    objG.CalcularSalario(int.Parse(txtHoras.Text));
                    lblRes.Text = Convert.ToString(objG.Salario());
                    break;
                case 4:
                    objD.CalcularSalario(int.Parse(txtHoras.Text));
                    lblRes.Text = Convert.ToString(objD.Salario());
                    break;
            }

            lbl1.Visible = true;
            lblRes.Visible = true;
        }
    }
}
