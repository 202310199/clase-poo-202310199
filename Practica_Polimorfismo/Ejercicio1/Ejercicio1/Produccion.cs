﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Produccion : Empleado
    {
        public override void CalcularSalario(int h)
        {
            salario = h * 35;
        }

        public override int Salario()
        {
            return salario;
        }
    }
}
