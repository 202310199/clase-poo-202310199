﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_PolimorfismoHerencia
{
    class Rectangulo : Figuras
    {
        public override void CalcularArea(double largo, double altura)
        {
            area = largo * altura;
            MessageBox.Show("El area del rectángulo es: " + Convert.ToString(area));
        }
    }
}
