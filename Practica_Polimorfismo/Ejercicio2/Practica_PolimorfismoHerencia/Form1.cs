﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_PolimorfismoHerencia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Rectangulo objR = new Rectangulo();
        Circunferencia objC = new Circunferencia();
        Triangulo objT = new Triangulo();

        private void btnCalculaArea_Click(object sender, EventArgs e)
        {
            if (radRectangulo.Checked)
            {
                objR.CalcularArea(double.Parse(txtLargo.Text), double.Parse(txtAltura.Text));
            }

            if (radTriangulo.Checked)
            {
                objT.CalcularArea(double.Parse(txtLargo.Text), double.Parse(txtAltura.Text));
            }

            if (radCircunferencia.Checked)
            {
                objC.CalcularArea(double.Parse(txtRadio.Text), double.Parse(txtRadio.Text));
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
