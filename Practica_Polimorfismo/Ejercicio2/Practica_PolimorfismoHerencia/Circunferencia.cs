﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_PolimorfismoHerencia
{
    class Circunferencia : Figuras
    {
        public override void CalcularArea(double radio, double r)
        {
            area = (radio * r) * Math.PI;
            MessageBox.Show("El area de la circunferencia es: " + Convert.ToString(area));
        }
    }
}
