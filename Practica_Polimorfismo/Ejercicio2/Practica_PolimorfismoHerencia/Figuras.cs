﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_PolimorfismoHerencia
{
    class Figuras
    {
        protected double area = 0;

        public virtual void CalcularArea(double a, double b)
        {
            MessageBox.Show("El area es {0}", Convert.ToString(area));
        }
    }
}
