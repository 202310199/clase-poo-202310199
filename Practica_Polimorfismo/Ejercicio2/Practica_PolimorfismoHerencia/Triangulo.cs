﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_PolimorfismoHerencia
{
    class Triangulo : Figuras
    {
        public override void CalcularArea(double largo, double altura)
        {
            area = (largo * altura)/2;
            MessageBox.Show("El area del triángulo es: " + Convert.ToString(area));
        }
    }
}
