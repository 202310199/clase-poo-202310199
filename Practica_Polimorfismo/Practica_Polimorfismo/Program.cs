﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            double sueldohora, horas, ventas, tarifa, sueldoBase;
            string nombre, apellido, seguro;

            Console.WriteLine("---------------SALARIO SEMANAL POR TIPO DE EMPLEADO---------------\n");

            Console.Write("Nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Apellido: ");
            apellido = Console.ReadLine();
            Console.Write("Numero de seguro social: ");
            seguro = Console.ReadLine();

            Empleado_Asalariado obj = new Empleado_Asalariado();
            obj.Datos(nombre, apellido, seguro);

            Console.WriteLine("\nElige una de las siguientes opciones: \n");
            Console.WriteLine("1.- Empleado por horas");
            Console.WriteLine("2.- Empleado por comision");
            Console.WriteLine("3.- Empleado con sueldo base mas comision");
            int seleccion = int.Parse(Console.ReadLine());

            switch (seleccion)
            {
                case 1:
                    Console.Write("Sueldo por hora: ");
                    sueldohora = int.Parse(Console.ReadLine());
                    Console.Write("Horas trabajadas: ");
                    horas = int.Parse(Console.ReadLine());
                    EmpleadoPorHoras objH = new EmpleadoPorHoras(horas, sueldohora);
                    objH.CalculaSalario();
                    break;
                case 2:
                    Console.Write("Ventas Brutas: ");
                    ventas = int.Parse(Console.ReadLine());
                    Console.Write("Tarifa por comision: ");
                    tarifa = int.Parse(Console.ReadLine());
                    EmpleadoPorComisiones objC = new EmpleadoPorComisiones(ventas, tarifa);
                    objC.CalculaSalario();
                    break;
                case 3:
                    Console.Write("Ventas Brutas: ");
                    ventas = int.Parse(Console.ReadLine());
                    Console.Write("Tarifa por comision: ");
                    tarifa = int.Parse(Console.ReadLine());
                    Console.Write("Sueldo Base: ");
                    sueldoBase = double.Parse(Console.ReadLine());
                    EmpleadoBaseMasComision objBM = new EmpleadoBaseMasComision(tarifa, ventas, sueldoBase);
                    objBM.CalculaSalario();
                    break;
                default:
                    Console.WriteLine("Opcion no identificada");
                    break;
            }

            Console.ReadKey();
        }
    }
}
