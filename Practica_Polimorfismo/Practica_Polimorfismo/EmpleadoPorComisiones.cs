﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Polimorfismo
{
    class EmpleadoPorComisiones : Empleado_Asalariado, Empleado
    {
        private double ventasBrutas, tarifaComision;

        public EmpleadoPorComisiones(double v, double t)
        {
            ventasBrutas = v;
            tarifaComision = t;
        }

        public void CalculaSalario()
        {
            salariosemanal = tarifaComision * ventasBrutas;
            Console.WriteLine("El empleado " + primerNombre + apellidoPaterno + " tiene un salario semanal de : " + salariosemanal);
        }
    }
}
