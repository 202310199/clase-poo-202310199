﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Polimorfismo
{
    class EmpleadoBaseMasComision : Empleado_Asalariado, Empleado
    {
        private double tarifaComision, ventasBrutas, salarioBase;

        public EmpleadoBaseMasComision(double t, double v, double s)
        {
            tarifaComision = t;
            ventasBrutas = v;
            salarioBase = s;
        }

        public void CalculaSalario()
        {
            salariosemanal = (tarifaComision * ventasBrutas) + salarioBase;
            Console.WriteLine("El empleado " + primerNombre + apellidoPaterno + " tiene un salario semanal de : " + salariosemanal);
        }
    }
}
