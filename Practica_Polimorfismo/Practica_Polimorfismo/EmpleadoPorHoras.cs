﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Polimorfismo
{
    class EmpleadoPorHoras : Empleado_Asalariado, Empleado
    {
        private double hora, sueldo;

        public EmpleadoPorHoras(double h, double s)
        {
            hora = h;
            sueldo = s;
        }

        public void CalculaSalario()
        {
            if (hora <= 40)
            {
                salariosemanal = sueldo * hora;
            }
            else
            {
                salariosemanal = (40 * sueldo) + (hora - sueldo) * sueldo * 1.5;
            }

            Console.WriteLine("El empleado " + primerNombre + apellidoPaterno + " tiene un salario semanal de : " + salariosemanal);
        }
    }
}
