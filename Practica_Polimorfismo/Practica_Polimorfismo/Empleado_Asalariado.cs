﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Polimorfismo
{
    class Empleado_Asalariado
    {
        protected string primerNombre, apellidoPaterno, noSeguro;
        protected double salariosemanal;

        public void Datos(string p, string a, string ns)
        {
            primerNombre = p;
            apellidoPaterno = a;
            noSeguro = ns;
        }
    }
}
