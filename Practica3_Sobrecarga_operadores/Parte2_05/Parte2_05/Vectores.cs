﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_05
{
    class Vectores
    {
        short[] v1 = new short[20];
        long[] v2 = new long[20];

        //se llenan los arreglos
        public void LlenarArreglos()
        {
            Console.WriteLine("Llenar el arreglo tipo short");
            for (int i = 0; i < v1.Length; i++)
            {
                Console.Write("Ingrese valor: ");
                v1[i] = short.Parse(Console.ReadLine());
            }
            Console.WriteLine();
            Console.WriteLine("Llenar el arreglo tipo long");
            for (int i = 0; i < v2.Length; i++)
            {
                Console.Write("Ingrese valor: ");
                v2[i] = long.Parse(Console.ReadLine());
            }
        }

        //determina el numero mayor en cada arreglo y la posicion de dicho numero
        public void Mayor()
        {
            int p1 = 0, p2 = 0;
            short m1 = 0;
            long m2 = 0;

            for (int i = 0; i < 20; i++)
            {
                if (v1[i] > m1)
                {
                    m1 = v1[i];
                    p1 = i;
                }

                if (v2[i] > m2)
                {
                    m2 = v2[i];
                    p2 = i;
                }
            }

            Console.WriteLine("El numero mayor del arreglo tipo short es {0} y su posición en el arreglo es {1}", m1, p1);
            Console.WriteLine("El numero mayor del arreglo tipo long es {0} y su posición en el arreglo es {1}", m2, p2);
        }
    }
}
