﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_05
{
    class Program
    {
        static void Main(string[] args)
        {
            //instanciacion de la clsae vectores
            Vectores obj = new Vectores();

            //se manda llamar a los metodos
            obj.LlenarArreglos();
            Console.WriteLine();
            obj.Mayor();

            Console.ReadKey();
        }
    }
}
