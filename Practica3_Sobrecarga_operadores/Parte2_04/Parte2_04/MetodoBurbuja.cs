﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_04
{
    class MetodoBurbuja
    {
        //Atributos
        int[] edad;
        int n;

        //Se aggregan valores al arreglo
        public void LlenarArray()
        {
            Console.Write("Tamaño del arreglo: ");
            n = int.Parse(Console.ReadLine());
            edad = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Ingrese una edad: ");
                edad[i] = int.Parse(Console.ReadLine());
            }
        }

        //Se ordena el array con el metodo burbuja
        public void Burbuja()
        {
            int T;

            for (int i = 1; i < n; i++)
            {
                for (int j = 1; j < n; j++)
                {
                    if (edad[j - 1] > edad[j])
                    {
                        T = edad[j - 1];
                        edad[j - 1] = edad[j]; 
                        edad[j] = T;
                    }
                }
            }
        }

        //Se imprime el array
        public void MostrarArray()
        {
            Console.WriteLine("Arreglo después del método burbuja:");

            foreach (var item in edad)
            {
                Console.WriteLine(item);
            }
        }
    }
}
