﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-----METODO BURBUJA-----");
            Console.WriteLine();

            //Instanciacion de la clase
            MetodoBurbuja obj = new MetodoBurbuja();

            //Se mandan a llamar a los metodos
            obj.LlenarArray();
            Console.WriteLine();
            obj.Burbuja();
            Console.WriteLine();
            obj.MostrarArray();

            Console.ReadKey();
        }
    }
}
