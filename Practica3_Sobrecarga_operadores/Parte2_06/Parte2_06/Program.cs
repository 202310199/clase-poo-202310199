﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_06
{
    class Program
    {
        static void Main(string[] args)
        {
            //se instancia la clase Vector
            Vector obj = new Vector();

            //se manda a llamar a los metodos
            obj.LlenarVector();
            obj.Imprimir();

            Console.ReadKey();
        }
    }
}
