﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_06
{
    class Vector
    {
        int[] vector = new int[300];

        //se llena el arreglo con numeros aleatorios
        public void LlenarVector()
        {
            Random llenar = new Random();

            for (int i = 0; i < vector.Length; i++)
            {
                vector[i] = llenar.Next(-300, 300);
            }
        }

        //se imprime la cantidad de 0, numeros negativos y positivos
        //se imprime la suma de los numeros positivos y la suma de los numeros negativos
        public void Imprimir()
        {
            int cero = 0, negativos = 0, positivos = 0;
            int sumaPositivos = 0, sumaNegativos = 0;

            for (int i = 0; i < vector.Length; i++)
            {
                if(vector[i] == 0)
                {
                    cero++;
                }
                if (vector[i] > 0)
                {
                    positivos++;
                    sumaPositivos = sumaPositivos + vector[i];
                }
                if (vector[i] < 0)
                {
                    negativos++;
                    sumaNegativos = sumaNegativos + vector[i];
                }
            }

            Console.WriteLine("El arreglo contiene {0} ceros, {1} números positivos y {2} números negativos", cero, positivos, negativos);
            Console.WriteLine();
            Console.WriteLine("La suma de los números positivos es {0}", sumaPositivos);
            Console.WriteLine();
            Console.WriteLine("La suma de los números negativos es {0}", sumaNegativos);
        }
    }
}
