﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte1_01
{
    class OrdVar
    {
        //Ordena los valores de la matriz de mayor a menor y despues los muestra
        public void Ordenar(params int[] a)
        {
            Console.WriteLine("------------ORDENA------------");
            Array.Sort(a);

            foreach (int i in a)
            {
                Console.WriteLine(+i);
            }
        }
        //Ordena los valores de manera inversa a la que se guardaron y luego los muestra
        public void Ordenar(params float[] b)
        {
            Console.WriteLine("------------REORDENA----------");
            Array.Reverse(b);

            foreach (int i in b)
            {
                Console.WriteLine(+i);
            }
        }
    }
}
