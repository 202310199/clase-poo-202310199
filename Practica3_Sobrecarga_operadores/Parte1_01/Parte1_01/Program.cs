﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte1_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Atributos
            int[] a = new int[4];
            float[] b = new float[4];

            //Instanciación de la clase Ordvar
            OrdVar p = new OrdVar();

            //Pide los valores del arreglo a
            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            //Pide los valores del arreglo b
            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = Convert.ToInt32(Console.ReadLine());
            }

            //Llama a los métodos de la clase Ordvar
            p.Ordenar(a);
            p.Ordenar(b);

            Console.ReadLine();
        }
    }
}
