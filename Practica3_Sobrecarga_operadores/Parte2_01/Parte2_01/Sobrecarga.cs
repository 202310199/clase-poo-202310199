﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_01
{
    class Sobrecarga
    {
        //Sobrecarga de metodos - devuelven el valor mayor
        public void Grande(int a, int b)
        {
            Console.WriteLine(Math.Max(a, b));
        }

        public void Grande(int a, int b, int c)
        {
            if (a>b && a>c)
            {
                Console.WriteLine(a);
            }
            else
            {
                if (b>a && b>c)
                {
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine(c);
                }
            }
        }

        public void Grande(int a, int b, int c, int d)
        {
            if (a>b && a>c && a>d)
            {
                Console.WriteLine(a);
            }
            else
            {
                if (b>a && b>c && b>d)
                {
                    Console.WriteLine(b);
                }
                else
                {
                    if (c>a && c>b && c>d)
                    {
                        Console.WriteLine(c);
                    }
                    else
                    {
                        Console.WriteLine(d);
                    }
                }
            }
        }
    }
}
