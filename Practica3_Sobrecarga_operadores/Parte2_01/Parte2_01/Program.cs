﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciación de la clase Sobrecarga
            Sobrecarga obj = new Sobrecarga();

            //Se manda llamar los metodos y se pasa parametros
            obj.Grande(10, 4);
            obj.Grande(15, 35, 23);
            obj.Grande(10, 12, 12, 18);

            Console.ReadKey();
        }
    }
}
