﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_02
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciacion de la clase Exponente
            Exponente obj = new Exponente();

            Console.Write("Ingrese un valor de tipo entero:");
            int v1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Elevado al cuadrado es: " + obj.ElevarCuadrado(v1));

            Console.Write("Ingrese un valor de tipo flotante:");
            float v2 = float.Parse(Console.ReadLine());
            Console.WriteLine("Elevado al cuadrado es: " + obj.ElevarCuadrado(v2));

            Console.Write("Ingrese un valor de tipo double:");
            double v3 = double.Parse(Console.ReadLine());
            Console.WriteLine("Elevado al cuadrado es: " + obj.ElevarCuadrado(v3));

            Console.ReadKey();
        }
    }
}
