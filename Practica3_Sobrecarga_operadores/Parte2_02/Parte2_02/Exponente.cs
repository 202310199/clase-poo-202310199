﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_02
{
    class Exponente
    {
        //Sobrecarga de metodos
        //Elevar al cuadrado numeros de dferente tipo de dato
        public int ElevarCuadrado(int v)
        {
            return v * v;
        }

        public float ElevarCuadrado(float v)
        {
            return v * v;
        }

        public double ElevarCuadrado(double v)
        {
            return v * v;
        }
    }
}
