﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_03
{
    class Datos
    {
        //Atributos
        private string[] nombres = new string[35];
        private string[] telefono = new string[35];

        //Método para guardar los datos en las matrices
        public void CapturaDatos()
        {
            Console.WriteLine("CAPTURA DE DATOS");
            for (int i = 0; i < 35; i++)
            { 
                Console.Write("Ingrese nombre: ");
                nombres[i] = (Console.ReadLine());
                Console.Write("Ingrese numero de telefono: ");
                telefono[i] = (Console.ReadLine());
            }
        }

        //Metodo para mostrar los datos guardados
        public void MuestraDatos()
        {
            Console.WriteLine("DATOS REGISTRADOS");
            for (int i = 0; i < 35; i++)
            {
                Console.Write("Nombre: " + nombres[i] + " Celular: " + telefono[i]);
                Console.WriteLine();
            }
        }
    }
}
