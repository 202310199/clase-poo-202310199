﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("REGISTRO DE ALUMNOS Y DOCENTES DE LA ESCUELA BUENAVENTURA");
            Console.ReadLine();

            //Instanciación de la clase Datos
            Datos obj = new Datos();

            //Se llama a los métodos
            obj.CapturaDatos();
            Console.WriteLine();
            obj.MuestraDatos();

            Console.ReadKey();
        }
    }
}
