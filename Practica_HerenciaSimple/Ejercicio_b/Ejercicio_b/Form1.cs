﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_b
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        AccionRealizada obj = new AccionRealizada();

        private void btnListar_Click(object sender, EventArgs e)
        {
            obj.listar();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            obj.actualizar(int.Parse(txtMonitoreo.Text), int.Parse(txtPulso.Text), int.Parse(txtPresion.Text), int.Parse(txtContracciones.Text),
                int.Parse(txtConvulsiones.Text));
            obj.actualizar2(txtDiagnostico.Text, txtHemorragia.Text, txtParo.Text, txtOtros.Text, txtCodigo.Text, txtDescripcion.Text, txtTipo.Text);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            obj.guardar(int.Parse(txtMonitoreo.Text), int.Parse(txtPulso.Text), int.Parse(txtPresion.Text), int.Parse(txtContracciones.Text),
               int.Parse(txtConvulsiones.Text));
            obj.guardar2(txtDiagnostico.Text, txtHemorragia.Text, txtParo.Text, txtOtros.Text, txtCodigo.Text, txtDescripcion.Text, txtTipo.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
