﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_b
{
    class AccionRealizada : MonitoreoViaje
    {
        private string cod_accion, descripcion, tipo;

        public void listar()
        {
            MessageBox.Show("-Número de monitoreo: " + nro_monitoreo + "\n-Pulso: " + pulso +"\n-Presion: " + presion +
                "\n-Número de contracciones:" + nro_contracciones + "\n-Convulsiones por hora: " + convulsiones_hora +
                "\n-Diagnostico: " + diagnostico_pac + "\n-Hemorragia: " + hem_cantidad + "\n-Paro cardiaco: " + paro_cardio +
                "\n-Otros: " + otros + "\n-Codigo de acción: " + cod_accion + "\nDescripción: " + descripcion + "\n-Tipo: " + tipo);
        }

        public void actualizar(int nroM, int pu, int pr, int nroC, int c)
        {
            this.nro_monitoreo = nroM;
            this.pulso = pu;
            this.presion = pr;
            this.nro_contracciones = nroC;
            this.convulsiones_hora = c;
        }

        public void actualizar2(string d, string h, string pa, string o, string cod, string desc, string t)
        {
            this.diagnostico_pac = d;
            this.hem_cantidad = h;
            this.paro_cardio = pa;
            this.otros = o;
            this.cod_accion = cod;
            this.descripcion = desc;
            this.tipo = t;
        }

        public void guardar(int nroM, int pu, int pr, int nroC, int c)
        {
            this.nro_monitoreo = nroM;
            this.pulso = pu;
            this.presion = pr;
            this.nro_contracciones = nroC;
            this.convulsiones_hora = c;
        }

        public void guardar2(string d, string h, string pa, string o, string cod, string desc, string t)
        {
            this.diagnostico_pac = d;
            this.hem_cantidad = h;
            this.paro_cardio = pa;
            this.otros = o;
            this.cod_accion = cod;
            this.descripcion = desc;
            this.tipo = t;
        }
    }
}
