﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Punto2D
    {
        public int x;
        public int y;
    }

    class Punto3D : Punto2D
    {
        public int z;
    }
}
