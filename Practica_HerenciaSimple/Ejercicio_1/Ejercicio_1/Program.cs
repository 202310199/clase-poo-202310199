﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instancia las clases
            Punto2D p2d = new Punto2D();
            Punto3D p3d = new Punto3D();

            //asigna valores a los atributos y los muestra en pantalla
            Console.WriteLine(p2d.x = 100);
            Console.WriteLine(p2d.y = 200);
            Console.WriteLine(p3d.x = 150);
            Console.WriteLine(p3d.y = 250);
            Console.WriteLine(p3d.z = 350);

            Console.ReadLine();
        }
    }
}
