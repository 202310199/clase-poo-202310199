﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_a
{
    public partial class frmReserva : Form
    {
        public frmReserva()
        {
            InitializeComponent();
        }

        //Instanciacion de la clase vuelo
        Vuelo objV = new Vuelo();

        //si se elige hacer una reservacion apareceran los siguientes campos
        private void btnReserva_Click(object sender, EventArgs e)
        {
            lblDestino.Visible = true;
            txtDestino.Visible = true;
            lblFecha.Visible = true;
            txtFecha.Visible = true;
            lblNumero.Visible = true;
            txtNumero.Visible = true;
            lblPrecio.Visible = true;
            txtPrecio.Visible = true;
            lblSalida.Visible = true;
            txtSalida.Visible = true;
            lblTiempo.Visible = true;
            txtTiempo.Visible = true;
            btnGuardar.Visible = true;
            btnMostrar.Visible = true;
            btnSalir.Visible = true;
            btnSalida.Visible = false;
        }

        //Si se elige no hacer reservacion se cierra el programa
        private void btnSalida_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Se obtiene los valores de los atributos de la clase vuelo apartir de los metodos
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            objV.DatosCadena(txtDestino.Text, txtSalida.Text, txtFecha.Text, txtTiempo.Text);
            objV.DatosInt(int.Parse(txtPrecio.Text), int.Parse(txtNumero.Text));
        }

        //El boton mostrar manda a llamar al metodo Mostrar
        private void btnMostrar_Click(object sender, EventArgs e)
        {
            objV.Mostrar();
        }

        //El boton salir cierra el programa
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmReserva_Load(object sender, EventArgs e)
        {

        }
    }
}
