﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_a
{
    class Vuelo : Aerolinea
    {
        string destino, salida, fecha, tiempo;
        int precio, numero;

        //si el atributo heredado "dispReserva" es true
        //obtiene valores para los atributos de tipo string
        public void DatosCadena(string d, string s, string f, string t )
        {
            if (dispReserva == true)
            {
                this.destino = d;
                this.salida = s;
                this.fecha = f;
                this.tiempo = t;
            }
        }

        //si el atributo heredado "dispReserva" es true
        //obtiene valores para los atributos de tipo int
        public void DatosInt(int p, int n)
        {
            if (dispReserva == true)
            {
                this.precio = p;
                this.numero = n;
            }
        }

        //muestra los datos guardados
        public void Mostrar()
        {
            MessageBox.Show("Vuelo #" + numero + " con destino " + destino + " y salida de " + salida +
                ", duración de " + tiempo + ", fecha " + fecha + ", costo " + precio);
        }
    }
}
