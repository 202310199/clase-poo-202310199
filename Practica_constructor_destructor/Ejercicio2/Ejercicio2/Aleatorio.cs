﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Aleatorio
    {
        private int[] aleatorio = new int[10];
        private Random al = new Random();
        private int c;

        public Aleatorio(int c)
        {
            this.c = c;
        }

        public void ImprimeAleatorio()
        {
            for (int i = 0; i < c; i++)
            {
                aleatorio[i] = al.Next(250);
            }

            foreach (var item in aleatorio)
            {
                Console.WriteLine(item);
            }
        }

        ~Aleatorio()
        {
            c = 0;
        }
    }
}
