﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_3
{
    class Primo
    {
        private int numero;

        public Primo(int n)
        {
            this.numero = n;
        }

        public void ImprimePrimo()
        {
            if (numero  %2 == 0 && numero !=2)
            {
                MessageBox.Show("No es número primo");
            }
            else
            {
                MessageBox.Show("Es número primo");
            }
        }

        ~Primo()
        {
            numero = 0;
        }
    }
}
