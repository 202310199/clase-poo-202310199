﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Prueba
    {
        private int x;

        public Prueba(int x)
        {
            this.x = x;
            System.Console.Write("Creando objeto prueba con x={0}", x);
        }

        ~Prueba()
        {
            this.x = 0;
        }
    }
}
