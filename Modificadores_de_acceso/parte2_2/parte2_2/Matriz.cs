﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parte2_2
{
    class Matriz
    {
        private int[,] a;
        public int n, m;

        public void LeerDatos()
        {
            Console.Write("Ingrese el valor de n: ");
            n = int.Parse(Console.ReadLine());
            Console.Write("Ingrese el valor de m: ");
            m = int.Parse(Console.ReadLine());
            a = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("Ingrese valor para la posición {0},{1}: ", i, j);
                    a[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void MostrarDatos()
        {
            Console.WriteLine("Valores contenido en la matriz:");

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("   " + a[i, j] + "   ");
                }
                Console.WriteLine();
            }
        }
    }
}
