﻿
namespace parte2_4._3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.EDAD = new System.Windows.Forms.TextBox();
            this.MESES = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edad";
            // 
            // EDAD
            // 
            this.EDAD.Location = new System.Drawing.Point(104, 94);
            this.EDAD.Name = "EDAD";
            this.EDAD.Size = new System.Drawing.Size(100, 20);
            this.EDAD.TabIndex = 1;
            // 
            // MESES
            // 
            this.MESES.AutoSize = true;
            this.MESES.Location = new System.Drawing.Point(297, 97);
            this.MESES.Name = "MESES";
            this.MESES.Size = new System.Drawing.Size(0, 13);
            this.MESES.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(171, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 254);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.MESES);
            this.Controls.Add(this.EDAD);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox EDAD;
        private System.Windows.Forms.Label MESES;
        private System.Windows.Forms.Button button1;
    }
}

