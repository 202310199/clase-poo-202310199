﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parte2_4._3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            metodo();
        }

        void metodo()
        {
            int edad = Int32.Parse(EDAD.Text);
            edad = edad * 12;
            MESES.Text = edad.ToString() + " meses";
        }
    }
}
