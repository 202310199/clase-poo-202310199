﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parte2_3
{
    class Promedio
    {
        int Cal1, Cal2, Cal3;
        double promedio;

        public void CalcPromedio()
        {
            Console.Write("Ingrese la calificación 1: ");
            Cal1 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese la calificación 2: ");
            Cal2 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese la calificación 3: ");
            Cal3 = int.Parse(Console.ReadLine());
            promedio = (Cal1 + Cal2 + Cal3) / 3;
        }

        public void MostrarPromedio()
        {
            Console.WriteLine("El promedio es {0}", promedio);
        }
    }
}
