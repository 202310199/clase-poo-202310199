﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parte2_1
{
    class Vector
    {
        static int[] a = new int[20];

        public void LeeDatos()
        {
            Console.WriteLine("Ingrese 20 numeros enteros:");

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = int.Parse(Console.ReadLine());
            }
        }

        public void MostrarDatos()
        {
            Console.WriteLine("Datos contenidos en a:");

            foreach (int item in a)
            {
                Console.WriteLine(item);
            }
        }
    }
}
