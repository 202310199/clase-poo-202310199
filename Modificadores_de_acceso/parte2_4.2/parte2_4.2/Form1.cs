﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parte2_4._2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] a = new int[10];

            for (int x = 0; x < 10; x++)
            {
                a[x] = x * 6;
                listBox1.Items.Add(a[x].ToString());
                Calcula(a);
                listBox2.Items.Add(a[x].ToString());
            }

            void Calcula(int[] b)
            {
                for (int x = 0; x < 10; x++)
                {
                    b[x] = b[x] * 12;
                }
            }
        }
    }
}
