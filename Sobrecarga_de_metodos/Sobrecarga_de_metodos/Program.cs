﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sobrecarga_de_metodos
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();
            int resultado = obj.Suma(8, 5);
            Console.WriteLine("El resultado del metodo 1 es: {0}", resultado);
            double res = obj.Suma(5.88, 7.33, 9.22);
            Console.WriteLine("El resultado del metodo 2 es: {0}", res);
            float r = obj.Suma(8.9f, 5.3f, 4.6f);
            Console.WriteLine("El resultado del metodo 3 es: {0}", r);
        }
    }
}
