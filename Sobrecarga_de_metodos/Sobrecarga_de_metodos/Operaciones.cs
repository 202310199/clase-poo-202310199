﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sobrecarga_de_metodos
{
    class Operaciones
    {
        public int Suma(int a, int b)
        {
            int resultado = a + b;
            return resultado;
        }

        public double Suma(double a, double b, double c)
        {
            double resultado = a + b + c;
            return resultado;
        }

        public float Suma(float a, float b, float c)
        {
            float resultado = a + b + c;
            return resultado;
        }
    }
}
