﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Saludos : Bienvenida
    {
        private int num;

        public Saludos(int n)
        {
            this.num = n;
        }

        public override void TipoSaludo()
        {
            if (num == 1)
            {
                Console.WriteLine("**HOLA :)**");
            }
            else
            {
                if (num == 2)
                {
                    Console.WriteLine("**BONITO DIA**");
                }
                else
                {
                    if (num == 3)
                    {
                        Console.WriteLine("**BUENAS TARDES**");
                    }
                    else
                    {
                        Console.WriteLine("Opcion no valida");
                    }
                }
            }
        }

        public override void TipoDespedida()
        {
            if (num == 1)
            {
                Console.WriteLine("**TEN UN BUEN DIA**");
            }
            else
            {
                if (num == 2)
                {
                    Console.WriteLine("**HASTA LUEGO**");
                }
                else
                {
                    if (num == 3)
                    {
                        Console.WriteLine("**QUE TE VAYA BIEN**");
                    }
                    else
                    {
                        Console.WriteLine("Opcion no valida");
                    }
                }
            }
        }


    }
}
