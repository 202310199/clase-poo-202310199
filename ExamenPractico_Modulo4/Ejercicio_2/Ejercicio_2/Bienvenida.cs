﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    abstract class Bienvenida
    {
        public abstract void TipoSaludo();

        public abstract void TipoDespedida();
    }
}
