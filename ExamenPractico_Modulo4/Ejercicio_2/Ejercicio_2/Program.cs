﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ingrese un numero del 1 al 3: ");
            int num = int.Parse(Console.ReadLine());

            Saludos obj = new Saludos(num);
            obj.TipoSaludo();
            obj.TipoDespedida();

            Console.ReadKey();
        }
    }
}
