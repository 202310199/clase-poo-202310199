﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Volumen obj = new Volumen();

            Console.WriteLine("***************VOLUMEN CUBO***************\n");
            Console.WriteLine("Elige una de las opciones siguientes:\n");
            Console.WriteLine("1.- Calcular volumen con numeros enteros\n");
            Console.WriteLine("2.- Calcular volumen con numeros decimales\n");
            int e = int.Parse(Console.ReadLine());

            switch (e)
            {
                case 1:
                    Console.Write("Largo: ");
                    int largo = int.Parse(Console.ReadLine());
                    Console.Write("Ancho: ");
                    int ancho = int.Parse(Console.ReadLine());
                    Console.Write("Altura: ");
                    int alto = int.Parse(Console.ReadLine());
                    obj.CalculaVolumen(largo, ancho, alto);
                    break;
                case 2:
                    Console.Write("Largo: ");
                    double l = double.Parse(Console.ReadLine());
                    Console.Write("Ancho: ");
                    double a = double.Parse(Console.ReadLine());
                    Console.Write("Altura: ");
                    double al = double.Parse(Console.ReadLine());
                    obj.CalculaVolumen(l, a, al);
                    break;
                default:
                    Console.WriteLine("Opciòn seleccionada no valida");
                    break;
            }

            Console.ReadKey();
        }
    }
}
