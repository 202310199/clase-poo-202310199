﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Volumen
    {
        int volumen;
        double vol;

        public void CalculaVolumen(int largo, int ancho, int alto)
        {
            volumen = largo * ancho * alto;
            Console.WriteLine("\nEl volumen del cubo es: {0}", volumen);
        }

        public void CalculaVolumen(double largo, double ancho, double alto)
        {
            vol = largo * ancho * alto;
            Console.WriteLine("\nEl volumen del cubo es: {0}", vol);
        }
    }
}
