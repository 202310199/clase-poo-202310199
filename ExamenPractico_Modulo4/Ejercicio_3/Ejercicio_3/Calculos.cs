﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Calculos : Numeros
    {
        double suma = 0, promedio = 0;
        double[] matriz = new double[5];

        public void Matriz()
        {
            Console.WriteLine("\nIngrese 5 numeros: ");
            for (int i = 0; i < matriz.Length; i++)
            {
                matriz[i] = double.Parse(Console.ReadLine());
            }
        }

        public void SumaTotal()
        {
            for (int i = 0; i < matriz.Length; i++)
            {
                suma = suma + matriz[i];
            }

            Console.WriteLine("\nLa suma de los numeros ingresados es: {0}", suma);
        }

        public void Promedio()
        {
            promedio = suma / matriz.Length;
            Console.WriteLine("\nEl promedio de los numeros ingresados es: {0}", promedio);
        }

    }
}
