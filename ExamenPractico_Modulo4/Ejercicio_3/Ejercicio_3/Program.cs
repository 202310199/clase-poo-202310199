﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***************SUMA Y PROMEDIO***************");

            Calculos obj = new Calculos();
            obj.Matriz();
            obj.SumaTotal();
            obj.Promedio();

            Console.ReadKey();
        }
    }
}
