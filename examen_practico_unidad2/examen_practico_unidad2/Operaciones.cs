﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen_practico_unidad2
{
    class Operaciones
    {
        private double num1, num2;
        private double division;

        public Operaciones (double n1, double n2)
        {
            this.num1 = n1;
            this.num2 = n2;
        }

        private void Div()
        {
            division = num1 / num2;
        }

        public double getDivision()
        {
            return num1 / num2;
        }

        public double getSuma()
        {
            return num1 + num2;
        }

        public double getResta()
        {
            return num1 - num2;
        }

        public double getMultiplicacion()
        {
            return num1 * num2;
        }
    }
}
