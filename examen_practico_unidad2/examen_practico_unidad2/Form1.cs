﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examen_practico_unidad2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            Operaciones objDiv = new Operaciones(double.Parse(txtNum1.Text), double.Parse(txtNum2.Text));
            txtResultado.Text=Convert.ToString(objDiv.getDivision());
            lblSigno.Text = "/";
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            Operaciones objSuma = new Operaciones(double.Parse(txtNum1.Text), double.Parse(txtNum2.Text));
            txtResultado.Text = Convert.ToString(objSuma.getSuma());
            lblSigno.Text = "+";
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            Operaciones objResta = new Operaciones(double.Parse(txtNum1.Text), double.Parse(txtNum2.Text));
            txtResultado.Text = Convert.ToString(objResta.getResta());
            lblSigno.Text = "-";
        }

        private void btnMultiplicacion_Click(object sender, EventArgs e)
        {
            Operaciones objMult = new Operaciones(double.Parse(txtNum1.Text), double.Parse(txtNum2.Text));
            txtResultado.Text = Convert.ToString(objMult.getMultiplicacion());
            lblSigno.Text = "x";
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNum1.Clear();
            txtNum2.Clear();
            txtResultado.Clear();
            lblSigno.Text = "";
        }
    }
}
