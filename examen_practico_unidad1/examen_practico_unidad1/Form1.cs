﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examen_practico_unidad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            casa objCasa = new casa();

            objCasa.setVentanas(9);
            int ventanasObj = objCasa.getVentanas();
            MessageBox.Show("La casa tiene " + ventanasObj + " ventanas");

            objCasa.setPuertas(3);
            int puertasObj = objCasa.getPuertas();
            MessageBox.Show("La casa tiene " + puertasObj + " puertas");

            objCasa.setColor("amarillo");
            string colorObj = objCasa.getColor();
            MessageBox.Show("La casa es de color " + colorObj);

            objCasa.setCuartos(4);
            int cuartosObj = objCasa.getCuartos();
            MessageBox.Show("La casa tiene " + cuartosObj + " cuartos");

            objCasa.setTamanio(16);
            int tamanioObj = objCasa.getTamanio();
            MessageBox.Show("La casa tiene " + tamanioObj + " metros cuadarados");

            objCasa.setMaterial("ladrillo");
            string materialObj = objCasa.getMaterial();
            MessageBox.Show("La casa es de " + materialObj);
        }
    }
}
