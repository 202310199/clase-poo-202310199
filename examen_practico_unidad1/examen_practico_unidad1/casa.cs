﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen_practico_unidad1
{
    class casa
    {
        public int ventanas = 0;
        public int puertas = 0;
        public string color = "";
        public int cuartos = 0;
        public int tamanio = 0;
        public string material = "";


        public void setVentanas(int v)
        {
            this.ventanas = v;
        }

        public int getVentanas()
        {
            return this.ventanas;
        }

        public void setPuertas(int p)
        {
            this.puertas = p;
        }

        public int getPuertas()
        {
            return this.puertas;
        }

        public void setColor(string co)
        {
            this.color = co;
        }

        public string getColor()
        {
            return this.color;
        }

        public void setCuartos(int cu)
        {
            this.cuartos = cu;
        }

        public int getCuartos()
        {
            return this.cuartos;
        }

        public void setTamanio(int t)
        {
            this.tamanio = t;
        }

        public int getTamanio()
        {
            return this.tamanio;
        }

        public void setMaterial(string m)
        {
            this.material = m;
        }

        public string getMaterial()
        {
            return this.material;
        }
    }
}
