﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metodos_Abstractos
{
    class Program
    {
        static void Main(string[] args)
        {
            Avion objAv = new Avion();
            Auto objAu = new Auto();

            objAv.Mantenimiento();
            objAu.Mantenimiento();

            Console.ReadKey();
        }
    }
}
