﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metodos_Abstractos
{
    abstract class Transporte
    {
        public abstract void Mantenimiento();
    }

    class Auto : Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de mantenimiento de auto\n Cambio de aceite\n Cambio de bujia\n Liquido de frenos\n Cambio de frenos");
        }
    }

    class Avion : Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de mantenimiento de avion\n Alas\n Pico\n Motor\n Llantas");
        }
    }
}
