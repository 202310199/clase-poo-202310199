﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int item = cbxOperacion.SelectedIndex; 
            Operaciones obj = new Operaciones(item);

            try
            {
                int n1 = int.Parse(txtNum1.Text);
                int n2 = int.Parse(txtNum2.Text);
                obj.Eleccion(int.Parse(txtNum1.Text), int.Parse(txtNum2.Text));
            }
            catch
            {
                MessageBox.Show("Los datos ingresados deben ser numeros enteros");
            }

            txtResultado.Text = Convert.ToString(obj.Resultado());
            
        }
    }
}
