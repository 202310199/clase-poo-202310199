﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Operaciones
    {
        private int num1, num2, seleccion;
        private double resultado;

        public Operaciones(int e)
        {
            this.seleccion = e;
        }

        public double Resultado()
        {
            return resultado;
        }

        public void Eleccion(int n1, int n2)
        {
            this.num1 = n1;
            this.num2 = n2;

            switch (seleccion)
            {
                case 0:
                    resultado = num1 + num2;
                    break;
                case 1:
                    resultado = num1 - num2;
                    break;
                case 2:
                    resultado = num1 / num2;
                    break;
                case 3:
                    resultado = num1 * num2;
                    break;
            }
        }
    }
}
