﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string nom, dir, tel, correo, carrera, turno;
        int edad;
        DateTime nacimiento;

        DatosFormulario obj = new DatosFormulario();

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtEdad.Clear();
            txtCarrera.Clear();
            txtCorreo.Clear();
            txtDireccion.Clear();
            txtFecha.Clear();
            txtNombre.Clear();
            txtTelefono.Clear();
            txtTurno.Clear();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            nom = txtNombre.Text;
            dir = txtDireccion.Text;
            tel = txtTelefono.Text;
            correo = txtCorreo.Text;
            carrera = txtCarrera.Text;
            turno = txtTurno.Text;

            try
            {
                nacimiento = Convert.ToDateTime(txtFecha.Text);
                edad = int.Parse(txtEdad.Text);
            }
            catch
            {
                MessageBox.Show("La fecha o edad no se ingresaron de manera correcta");
            }
            finally
            {
                MessageBox.Show("Revise que sus datos ingresados sean correctos para poder realizar su insrcipcion");
            }

            try
            {
                obj.GuardarDatos(nom, edad, dir, tel, correo, carrera, turno);
            }
            catch (Exception z)
            {
                MessageBox.Show(z.Message);
            }
            finally
            {
                MessageBox.Show("Revise y guarde su comprobante de inscripcion");
            }
            
            
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            obj.Mostrar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
