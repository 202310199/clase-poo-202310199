﻿
namespace Ejercicio_2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLunes = new System.Windows.Forms.RadioButton();
            this.radMartes = new System.Windows.Forms.RadioButton();
            this.radMiercoles = new System.Windows.Forms.RadioButton();
            this.radJueves = new System.Windows.Forms.RadioButton();
            this.radViernes = new System.Windows.Forms.RadioButton();
            this.radSabado = new System.Windows.Forms.RadioButton();
            this.radDomingo = new System.Windows.Forms.RadioButton();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.txtCarrera = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.txtTurno = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "FORMULARIO DE INSCRIPCION";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombres:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Edad:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(313, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fecha de Nacimiento (AAAA-MM-DD):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Direccion:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(169, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Telefono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(329, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Correo Electronico:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Carrera: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Semestre:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radDomingo);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.radSabado);
            this.panel1.Controls.Add(this.radViernes);
            this.panel1.Controls.Add(this.radJueves);
            this.panel1.Controls.Add(this.radMiercoles);
            this.panel1.Controls.Add(this.radMartes);
            this.panel1.Controls.Add(this.radLunes);
            this.panel1.Location = new System.Drawing.Point(202, 184);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(263, 83);
            this.panel1.TabIndex = 9;
            // 
            // radLunes
            // 
            this.radLunes.AutoSize = true;
            this.radLunes.Location = new System.Drawing.Point(3, 26);
            this.radLunes.Name = "radLunes";
            this.radLunes.Size = new System.Drawing.Size(60, 17);
            this.radLunes.TabIndex = 0;
            this.radLunes.TabStop = true;
            this.radLunes.Text = "Primero";
            this.radLunes.UseVisualStyleBackColor = true;
            // 
            // radMartes
            // 
            this.radMartes.AutoSize = true;
            this.radMartes.Location = new System.Drawing.Point(61, 26);
            this.radMartes.Name = "radMartes";
            this.radMartes.Size = new System.Drawing.Size(68, 17);
            this.radMartes.TabIndex = 1;
            this.radMartes.TabStop = true;
            this.radMartes.Text = "Segundo";
            this.radMartes.UseVisualStyleBackColor = true;
            // 
            // radMiercoles
            // 
            this.radMiercoles.AutoSize = true;
            this.radMiercoles.Location = new System.Drawing.Point(135, 26);
            this.radMiercoles.Name = "radMiercoles";
            this.radMiercoles.Size = new System.Drawing.Size(62, 17);
            this.radMiercoles.TabIndex = 2;
            this.radMiercoles.TabStop = true;
            this.radMiercoles.Text = "Tercero";
            this.radMiercoles.UseVisualStyleBackColor = true;
            // 
            // radJueves
            // 
            this.radJueves.AutoSize = true;
            this.radJueves.Location = new System.Drawing.Point(204, 26);
            this.radJueves.Name = "radJueves";
            this.radJueves.Size = new System.Drawing.Size(56, 17);
            this.radJueves.TabIndex = 3;
            this.radJueves.TabStop = true;
            this.radJueves.Text = "Cuarto";
            this.radJueves.UseVisualStyleBackColor = true;
            // 
            // radViernes
            // 
            this.radViernes.AutoSize = true;
            this.radViernes.Location = new System.Drawing.Point(24, 58);
            this.radViernes.Name = "radViernes";
            this.radViernes.Size = new System.Drawing.Size(56, 17);
            this.radViernes.TabIndex = 4;
            this.radViernes.TabStop = true;
            this.radViernes.Text = "Quinto";
            this.radViernes.UseVisualStyleBackColor = true;
            // 
            // radSabado
            // 
            this.radSabado.AutoSize = true;
            this.radSabado.Location = new System.Drawing.Point(90, 58);
            this.radSabado.Name = "radSabado";
            this.radSabado.Size = new System.Drawing.Size(52, 17);
            this.radSabado.TabIndex = 5;
            this.radSabado.TabStop = true;
            this.radSabado.Text = "Sexto";
            this.radSabado.UseVisualStyleBackColor = true;
            // 
            // radDomingo
            // 
            this.radDomingo.AutoSize = true;
            this.radDomingo.Location = new System.Drawing.Point(158, 58);
            this.radDomingo.Name = "radDomingo";
            this.radDomingo.Size = new System.Drawing.Size(60, 17);
            this.radDomingo.TabIndex = 6;
            this.radDomingo.TabStop = true;
            this.radDomingo.Text = "Octavo";
            this.radDomingo.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(12, 69);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(154, 20);
            this.txtNombre.TabIndex = 10;
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(172, 69);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(154, 20);
            this.txtEdad.TabIndex = 11;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(172, 131);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(154, 20);
            this.txtTelefono.TabIndex = 12;
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(12, 131);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(154, 20);
            this.txtDireccion.TabIndex = 13;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(332, 131);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(160, 20);
            this.txtCorreo.TabIndex = 14;
            // 
            // txtFecha
            // 
            this.txtFecha.Location = new System.Drawing.Point(332, 69);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(160, 20);
            this.txtFecha.TabIndex = 15;
            // 
            // txtCarrera
            // 
            this.txtCarrera.Location = new System.Drawing.Point(12, 200);
            this.txtCarrera.Name = "txtCarrera";
            this.txtCarrera.Size = new System.Drawing.Size(154, 20);
            this.txtCarrera.TabIndex = 16;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(15, 311);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 17;
            this.btnGuardar.Text = "Guardar Datos";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(263, 311);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 18;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(390, 311);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 19;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // txtTurno
            // 
            this.txtTurno.Location = new System.Drawing.Point(12, 265);
            this.txtTurno.Name = "txtTurno";
            this.txtTurno.Size = new System.Drawing.Size(154, 20);
            this.txtTurno.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Turno:";
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(136, 311);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(85, 23);
            this.btnMostrar.TabIndex = 22;
            this.btnMostrar.Text = "Comprobante";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 346);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.txtTurno);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtCarrera);
            this.Controls.Add(this.txtFecha);
            this.Controls.Add(this.txtCorreo);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radDomingo;
        private System.Windows.Forms.RadioButton radSabado;
        private System.Windows.Forms.RadioButton radViernes;
        private System.Windows.Forms.RadioButton radJueves;
        private System.Windows.Forms.RadioButton radMiercoles;
        private System.Windows.Forms.RadioButton radMartes;
        private System.Windows.Forms.RadioButton radLunes;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.TextBox txtCarrera;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TextBox txtTurno;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnMostrar;
    }
}

