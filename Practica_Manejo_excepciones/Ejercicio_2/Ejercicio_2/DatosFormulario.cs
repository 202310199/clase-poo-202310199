﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_2
{
    class DatosFormulario
    {
        private string nombre, direccion, telefono, correo, carrera, turno;
        private int edad;

        public void GuardarDatos(string n, int e, string d, string te, string co, string ca, string tu)
        {
            this.nombre = n;
            this.edad = e;
            this.direccion = d;
            this.telefono = te;
            this.correo = co;
            this.carrera = ca;
            this.turno = tu;

            if (edad <= 15)
            {
                throw new Exception("Se supone que debe tener mas de l6 años");
            }
        }

        public void Mostrar()
        {
            MessageBox.Show("El alumno " + nombre + " quedo correctamente inscrito en el semestre actual");
        }
    }
}
