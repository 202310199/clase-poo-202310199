﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Citas
    {
        string dia, descripcion;
        int hora, minuto;

        public Citas(string d, string de, int h, int m)
        {
            this.dia = d;
            this.descripcion = de;
            this.hora = h;
            this.minuto = m;
        }

        public void GuardarDatos()
        {
            if (hora >= 24 || minuto >= 60)
            {
                throw new ArgumentException(String.Format("La hora o los minutos ingresados no son correctos"));
            }
        }
    }
}
