﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------AGENDA DE CITAS---------{0}");

            Console.Write("Día de la cita: ");
            string d = Console.ReadLine();
            Console.Write("Hora (0-23):");
            int h = int.Parse(Console.ReadLine());
            Console.Write("Minutos (0-59):");
            int m = int.Parse(Console.ReadLine());
            Console.Write("Descrpción de la cita:");
            string de = Console.ReadLine();

            try
            {
                Citas obj = new Citas(d, de, h, m);
                obj.GuardarDatos();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("{0}, {1}", e.GetType().Name, e.Message);
            }

            Console.ReadKey();
        }
    }
}
