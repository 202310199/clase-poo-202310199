﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pr02_Ej01
{
    class Circunferencia
    {
        double radio = 0;
        double area = 0;
        double perimetro = 0;

        public void setRadio(double r)
        {
            this.radio = r;
        }

        public void CalculaArea()
        {
            area = 3.1416 * (radio * radio);
            MessageBox.Show("El area es: " + area);
        }

        public void CalculaPerimetro()
        {
            perimetro = 2 * 3.1416 * radio;
            MessageBox.Show("El perimetro es: " + perimetro);
        }
    }
}
