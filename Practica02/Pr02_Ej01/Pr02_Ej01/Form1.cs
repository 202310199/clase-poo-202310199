﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pr02_Ej01
{
    public partial class Form1 : Form
    {
        public static double radio;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnArea_Click(object sender, EventArgs e)
        {
            Circunferencia objarea = new Circunferencia();
            objarea.setRadio(double.Parse(txtRadio.Text));
            objarea.CalculaArea();
        }

        private void btnPerimetro_Click(object sender, EventArgs e)
        {
            Circunferencia objperimetro = new Circunferencia();
            objperimetro.setRadio(double.Parse(txtRadio.Text));
            objperimetro.CalculaPerimetro();
        }

        private void btnRueda_Click(object sender, EventArgs e)
        {
            Circunferencia objrueda = new Circunferencia();
            objrueda.setRadio(10);
            objrueda.CalculaArea();
            objrueda.CalculaPerimetro();
        }

        private void btnMoneda_Click(object sender, EventArgs e)
        {
            Circunferencia objmoneda = new Circunferencia();
            objmoneda.setRadio(1);
            objmoneda.CalculaArea();
            objmoneda.CalculaPerimetro();
        }
    }
}
