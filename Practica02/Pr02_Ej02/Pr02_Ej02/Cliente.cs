﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pr02_Ej02
{
    class Cliente
    {
        string Apaterno = "";
        string Amaterno = "";
        string cp = "";
        string Id = "";
        string nombre = "";
        string telefono = "";
        string telcasa = "";
        string telmovil = "";
        string direccion = "";

        public void setApaterno(string ap)
        {
            this.Apaterno = ap;
        }

        public void setAmaterno(string am)
        {
            this.Amaterno = am;
        }

        public void setCP(string cp)
        {
            this.cp = cp;
        }

        public void setID(string id)
        {
            this.Id = id;
        }

        public void setnombre(string n)
        {
            this.nombre = n;
        }

        public void setTelefono(string t)
        {
            this.telefono = t;
        }

        public void setTelCasa(string tc)
        {
            this.telcasa = tc;
        }

        public void setTelMovil(string tm)
        {
            this.telmovil = tm;
        }

        public void setDireccion(string d)
        {
            this.direccion = d;
        }


        public void EliminaCliente()
        {
            Apaterno = "";
            Amaterno = "";
            cp = "";
            Id = "";
            nombre = "";
            telefono = "";
            telcasa = "";
            telmovil = "";
            direccion = "";
        }

        public void InsertaCiente()
        {

        }

        public void MostrarCliente()
        {
            MessageBox.Show("Cliente: " + Id + " " + nombre + " " + Apaterno + " " + Amaterno + " " + direccion + " " + cp + " " + telefono + " " + telcasa + " " + telmovil);
        }
    }
}
