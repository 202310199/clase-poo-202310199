﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pr02_Ej02
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            Cliente objInsertar = new Cliente();
            objInsertar.setApaterno(txtApaterno.Text);
            objInsertar.setAmaterno(txtAmaterno.Text);
            objInsertar.setCP(txtCp.Text);
            objInsertar.setID(txtId.Text);
            objInsertar.setnombre(txtNombre.Text);
            objInsertar.setTelefono(txtTelefono.Text);
            objInsertar.setTelCasa(txtCasa.Text);
            objInsertar.setTelMovil(txtMovil.Text);
            objInsertar.setDireccion(txtDireccion.Text);
            objInsertar.InsertaCiente();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Cliente objEliminar = new Cliente();
            txtApaterno.Clear();
            txtAmaterno.Clear();
            txtCp.Clear();
            txtId.Clear();
            txtNombre.Clear();
            txtTelefono.Clear();
            txtCasa.Clear();
            txtMovil.Clear();
            txtDireccion.Clear();
            objEliminar.EliminaCliente();
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            Cliente objMostrar = new Cliente();
            objMostrar.setApaterno(txtApaterno.Text);
            objMostrar.setAmaterno(txtAmaterno.Text);
            objMostrar.setCP(txtCp.Text);
            objMostrar.setID(txtId.Text);
            objMostrar.setnombre(txtNombre.Text);
            objMostrar.setTelefono(txtTelefono.Text);
            objMostrar.setTelCasa(txtCasa.Text);
            objMostrar.setTelMovil(txtMovil.Text);
            objMostrar.setDireccion(txtDireccion.Text);
            objMostrar.MostrarCliente();
        }
    }
}
