﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Operacion
    {
        int m, n;
        int[,] matriz;

        public void ImprimeMatriz()
        {
            Console.WriteLine("Valores contenidos en el arreglo: ");

            foreach (var item in matriz) //recorre cada elemento de la matriz
            {
                Console.WriteLine(item); //imprime cada elemento
            }
        }

        public void LeerMatriz()
        {
            Console.WriteLine("Ingrese m:"); 
            m = int.Parse(Console.ReadLine()); //se guarda el valor ingresado en la variable m
            Console.WriteLine("Ingrese n:");
            n = int.Parse(Console.ReadLine()); //se guarda el valor ingresado en la variable n
            matriz = new int[m, n]; 

            for (int i = 0; i < m; i++) //recorre el indice m
            {
                for (int j = 0; j < n; j++) //reorre el indice n
                {
                    Console.WriteLine("Ingrese valor:");
                    matriz[i, j] = int.Parse(Console.ReadLine()); //se guarda el valor ingresado en la posicion (m,n) de lka matriz
                }
            }
        }
    }
}
