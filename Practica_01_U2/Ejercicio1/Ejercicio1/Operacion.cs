﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Operacion
    {
        int n; 
        int[] matriz; 

        public void LLenarMatriz()
        {
            Console.WriteLine("¿Cuántos valores tiene el arreglo?"); //pide el valor de n
            n = int.Parse(Console.ReadLine()); //se guarda en n
            matriz = new int[n]; //se asigna cantidad de valores que contendrá la matriz
            
            for (int i = 0; i < n; i++) //recorre la matriz
            {
                Console.WriteLine("Ingrese valor:"); 
                matriz[i] = int.Parse(Console.ReadLine()); //guarda el valor en la matriz en la posicion de i
            }
        }

        public void ImprimePares()
        {
            Console.WriteLine("Números pares en el arreglo: ");
            for (int i = 0; i < n; i++) //recorre la matriz
            {
                if (matriz[i] % 2 == 0) //Si al dividir entre 2 el valor de la matriz en la posición i y es igual a 0...
                {
                    Console.WriteLine(matriz[i]); //imprime el numero par
                }
            }
        }

        public int getSumaArreglo()
        {
            int suma = 0; //variable que contiene la suma de la matriz

            for (int i = 0; i < n; i++) //recorre la matriz
            {
                suma = suma + matriz[i]; //se suma el valor de la matriz en la posicion i a la suma
            }

            return suma; 
        }
    }
}
