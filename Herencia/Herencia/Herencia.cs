﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Herencia
    {
        //atributos
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        //metodo publico
        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 de la clase herencia");
        }

        //metodo al que solo la clase "herencia" puede acceder
        private void metodo2()
        {
            Console.WriteLine("Este metodo no es accesible fuera de la clase herencia");
        }

        //metodo al que la clase "herencia" e "hijo" pueden acceder
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de la clase Herencia tiene protected");
        }

        //metodo publico para que otras clases puedan acceder al "metodo2"
        public void AccesoPrivate()
        {
            metodo2();
        }
    }

    //clase derivada de la clase "herencia"
    class Hijo : Herencia
    {
        //metodo publico para que otras clases puedan acceder al "metodo3"
        public void accesoProtected()
        {
            metodo3();
        }
    }
}
