﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //instanciacion de la clase "hijo"
            Hijo obj = new Hijo();
            //metodos de la clase "hijo" que se mandaron llamar
            obj.metodo1();
            obj.accesoProtected();

            //instanciacion de la clase "herencia"
            Herencia obj2 = new Herencia();
            //metodo de la clase "herencia" que se mando llamar
            obj2.AccesoPrivate();

            Console.ReadKey();
        }
    }
}
